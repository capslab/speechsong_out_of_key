var shuffleSequence = seq("consent","background","practiceintro","practice_1","practice_2","practice_3","practice_4","intro", "catch_1", rshuffle(startsWith('ss')), "exit2");

var practiceItemTypes = ["practice"];
var counterOverride = 3;

var defaults = [
    "AcceptabilityJudgment", {
        as: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        presentAsScale: true,
        timeout: 3000,
        instructions: "Use number keys or click boxes to answer.",
        leftComment: "(Speech)", rightComment: "(Song)"
    },

    "Question", {
        hasCorrect: false
    },
    "Message", {
        hideProgressBar: true,
    },
    "Form", {
        hideProgressBar: true,
        continueOnReturn: false,
        saveReactionTime: false,
    }
];


var items = [

    ["consent", "Form", {consentRequired: true, html: {include: "consent.html" }} ],
    ["background", "Form", {consentRequired: false, html: {include: "background.html" }} ],
    ["exit2", "Form", {consentRequired: true, html: {include: "exit2.html" }} ],

["practiceintro", Message, {consentRequired: false,
                    html: ["div",
                            ["p", "Please put your headphones/earphones on, or make sure your speakers are working. You will hear a series of spoken phrases, each repeated eight times. After each phrase, you will have 3 seconds to indicate whether the phrase sounded more like speech or more like song. As the phrase is repeated your perception of the phrase may or may not change; either way just do your best to accurately report how song-like the phrase sounds after each repetition. After 3 seconds, if you have not responded, the program will automatically go on to the next phrase. You will start with four practice trials to give you an idea of how the experiment will work."]
                          ]}],



["intro", Message, {consentRequired: false,
                    html: ["div",
                            ["p", "The test will now begin. Again, you will hear a series of spoken phrases, each repeated eight times. After each phrase, you will have 3 seconds to indicate whether the phrase sounded more like speech or more like song. As the phrase is repeated your perception of the phrase may or may not change; either way just do your best to accurately report how song-like the phrase sounds after each repetition. After 3 seconds, if you have not responded, the program will automatically go on to the next phrase."]
                          ]}]];


for (i = 1; i < 5; i++) {
     var item1 = ["practice_" + i.toString() + ""];
     for (j = 0; j < 8; j++) {
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: "practice_" + i.toString() + ".html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like speech vs. song, using the scale below."});
}
    items.push(item1);
    }

    for (i = 1; i < 49; i++) {
    var item1 = ["ss_" + i.toString() + ""];
     for (j = 0; j < 8; j++) {
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: i.toString() + "_out_of_key.html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like speech vs. song, using the scale below."});
}
    items.push(item1);
    }

     for (i = 1; i < 2; i++) {
    var item1 = ["catch_" + i.toString() + ""];
     for (j = 0; j < 4; j++) {
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: "Catch_trial_" + i.toString() + "_speech.html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like speech vs. song, using the scale below."});
}
        for (j = 0; j < 4; j++) {
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: "Catch_trial_" + i.toString() + "_song.html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like speech vs. song, using the scale below."});
}
    items.push(item1);
    }

        for (i = 2; i < 5; i++) {
    var item1 = ["ss_catch_" + i.toString() + ""];
     for (j = 0; j < 4; j++) {
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: "Catch_trial_" + i.toString() + "_speech.html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like speech vs. song, using the scale below."});
}
        for (j = 0; j < 4; j++) {
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: "Catch_trial_" + i.toString() + "_song.html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like speech vs. song, using the scale below."});
}
    items.push(item1);
    }
